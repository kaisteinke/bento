feather.replace();

document.getElementById("backButton").addEventListener("click", () => {
    window.location.replace("index.html")
});

/**
 * 
 * @param {string} key the name of the variable you want to receive
 * @returns {string} data
 */
function loadData(key){
    return localStorage.getItem(key);
}

/**
 * 
 * @param {string} key the name of the variable
 * @param {string} value the value of the variables
 */
function SetData(key, value){
    localStorage.setItem(key, value);
}

function insertAfter(referenceNode, newNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function getVal(elem){
    return document.getElementById(elem).value;
}

document.getElementById("submit").addEventListener("click", () => {
    // Name
    SetData("username", getVal("username"));
    // API KEY
    SetData("apikey", getVal("apikey"));
    // Coordinates
    SetData("latitude", getVal("latitude"));
    SetData("longitude", getVal("longitude"));
    // Icons
    SetData("icons", (getVal("icons") == "null" ? 'Nord' : getVal("icons")));
    // Temp Unit
    SetData("unit", (getVal("unit") == "null" ? 'C' : getVal("unit")));
    // Color
    SetData("accentcolor", getVal("accentcolor"));
    SetData("accentcolor2", getVal("accentcolor2"));
    // font
    SetData("font-family", getVal("font-family"));
    SetData("font-url", getVal("font-url"));
    // Time Format
    var radios = document.getElementsByName('timeformat');
    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            
            SetData("timeformat", radios[i].value);
        }
    }

    // Time Format
    var radios2 = document.getElementsByName('hover');
    for (var ii = 0, length = radios2.length; ii < length; ii++) {
        if (radios2[ii].checked) {
            SetData("hover", radios2[ii].value);
        }
    }
    // Links
    // 1
    SetData("gridlink1", getVal("gridlink1"));
    SetData("featherlink1", getVal("featherlink1"));
    // 2
    SetData("gridlink2", getVal("gridlink2"));
    SetData("featherlink2", getVal("featherlink2"));
    // 3
    SetData("gridlink3", getVal("gridlink3"));
    SetData("featherlink3", getVal("featherlink3"));
    // 4
    SetData("gridlink4", getVal("gridlink4"));
    SetData("featherlink4", getVal("featherlink4"));
    // 5
    SetData("gridlink5", getVal("gridlink5"));
    SetData("featherlink5", getVal("featherlink5"));
    // 6
    SetData("gridlink6", getVal("gridlink6"));
    SetData("featherlink6", getVal("featherlink6"));

    // LIST LINKS
    var counter11 = 0;
    var els = document.getElementsByClassName("nicelink1");
    Array.prototype.forEach.call(els, function(el) {
        counter11++;
        SetData(`linklist${counter11}`, el.value);
    });

    SetData("linkicon2", getVal("linkicon2"));
    SetData("linkicon1", getVal("linkicon1"));

    var counter22 = 0;
    var els2 = document.getElementsByClassName("nicelink2");
    Array.prototype.forEach.call(els2, function(el) {
        counter22++;
        SetData(`link2list${counter22}`, el.value)
    });

    window.location.hash = "top";
    showToast("Successfully Updated.", "green");
    loadStuff();
});

var counter = 1;

document.getElementById("newlink1").addEventListener("click", () => {
    clickbutton1();
});

function clickbutton1(){
    let returnval = null;
    counter++;
    if(counter >= 10){
        showToast("Maximal 9 Links allowed!");
    } else{
        let newelem1 = document.createElement("div");
        newelem1.classList.add("descr");
        newelem1.innerHTML = "Link + Title (title,link):";
    
        let newelem2 = document.createElement("div");
        newelem2.classList.add("input");
        newelem2.id = "superinput" + counter;
        newelem2.innerHTML = `<input type="url" class="nicelink1" name="linklist${counter}" id="linklist${counter}">`;
        
        if(counter == 2){
            insertAfter(document.getElementById("super1"), newelem1);
            insertAfter(newelem1, newelem2);
        } else{
            insertAfter(document.getElementById(`superinput${counter - 1}`), newelem1);
            insertAfter(newelem1, newelem2);
        }
        returnval = `linklist${counter}`;
    }
    return returnval;
}

var counter2 = 1;

document.getElementById("newlink2").addEventListener("click", () => {
    clickbutton2();
});

function clickbutton2(){
    let returnval = null;
    counter2++;
    if(counter2 >= 10){
        showToast("Maximal 9 Links allowed!");
    } else{
        let new2elem1 = document.createElement("div");
        new2elem1.classList.add("descr");
        new2elem1.innerHTML = "Link + Title (title,link):";
    
        let new2elem2 = document.createElement("div");
        new2elem2.classList.add("input");
        new2elem2.id = "superinput2" + counter2;
        new2elem2.innerHTML = `<input type="url" class="nicelink2" name="link2list${counter2}" id="link2list${counter2}">`;

        returnval = `link2list${counter2}`;
        
        if(counter2 == 2){
            insertAfter(document.getElementById("super2"), new2elem1);
            insertAfter(new2elem1, new2elem2);
        } else{
            insertAfter(document.getElementById(`superinput2${counter2 - 1}`), new2elem1);
            insertAfter(new2elem1, new2elem2);
        }
    }
    return returnval;
}

/**
 * function to show toast
 * @param {string} msg message to show
 */
function showToast(msg, color = "red") {
    var x = document.getElementById("snackbar");
    x.className = `show ${color}`;
    x.innerHTML = msg;
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}

/**
 * function to load already defined settings
 */
 function loadSettings(){
    // ------------------ Personal ------------------
    // Name
    const username = loadData("username");
    if(username !== null)
        document.getElementById("username").value = username;

    // ------------------ Weather ------------------
    // API Key
    const apikey = loadData("apikey");
    if(apikey !== null)
        document.getElementById("apikey").value = apikey;

    // Latitude
    const latitude = loadData("latitude");
    if(latitude !== null)
        document.getElementById("latitude").value = latitude;

    // Longitude
    const longitude = loadData("longitude");
    if(longitude !== null)
        document.getElementById("longitude").value = longitude;
    
    // Weather Icons
    const icons = loadData("icons");
    if(icons !== null)
        document.getElementById("icons").value = icons;

    // Temperature Unit
    const unit = loadData("unit");
    if(unit !== null)
        document.getElementById("unit").value = unit;
    
    // ------------------ Accessibility ------------------
    // Accent Color
    const accentcolor = loadData("accentcolor");
    if(accentcolor !== null)
        document.getElementById("accentcolor").value = accentcolor;
    
    // Accent Color
    const accentcolor2 = loadData("accentcolor2");
    if(accentcolor2 !== null)
        document.getElementById("accentcolor2").value = accentcolor2;

    // font family
    const fontfamily = loadData("font-family");
    if(fontfamily !== null)
        document.getElementById("font-family").value = fontfamily;
    else
        document.getElementById("font-family").value = "Open Sans";

    // font url
    const fontUrl = loadData("font-url");
    if(fontUrl !== null)
        document.getElementById("font-url").value = fontUrl;
    else
        document.getElementById("font-url").value = "https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700&display=swap";
    
    // time format
    const time = loadData("timeformat");
    if(time !== null){
        if(loadData("timeformat") == "24")
            document.getElementById("twentyfourhour").checked = true;
        else
            document.getElementById("twelvehour").checked = true;
    }

    // hover effect
    const hover = loadData("hover");
    if(hover !== null){
        if(loadData("hover") == "yes")
            document.getElementById("yes").checked = true;
        else
            document.getElementById("no").checked = true;
    }

    // ------------------ Links ------------------
    // Grid 1
    const gridlink1 = loadData("gridlink1");
    if(gridlink1 !== null)
        document.getElementById("gridlink1").value = gridlink1;
    
    const featherlink1 = loadData("featherlink1");
    if(featherlink1 !== null)
        document.getElementById("featherlink1").value = featherlink1;

    // Grid 2
    const gridlink2 = loadData("gridlink2");
    if(gridlink2 !== null)
        document.getElementById("gridlink2").value = gridlink2;
    
    const featherlink2 = loadData("featherlink2");
    if(featherlink2 !== null)
        document.getElementById("featherlink2").value = featherlink2;
    
    // Grid 3
    const gridlink3 = loadData("gridlink3");
    if(gridlink3 !== null)
        document.getElementById("gridlink3").value = gridlink3;
    
    const featherlink3 = loadData("featherlink3");
    if(featherlink3 !== null)
        document.getElementById("featherlink3").value = featherlink3;
    
    // Grid 4
    const gridlink4 = loadData("gridlink4");
    if(gridlink4 !== null)
        document.getElementById("gridlink4").value = gridlink4;
    
    const featherlink4 = loadData("featherlink4");
    if(featherlink4 !== null)
        document.getElementById("featherlink4").value = featherlink4;
    
    // Grid 5
    const gridlink5 = loadData("gridlink5");
    if(gridlink5 !== null)
        document.getElementById("gridlink5").value = gridlink5;
    
    const featherlink5 = loadData("featherlink5");
    if(featherlink5 !== null)
        document.getElementById("featherlink5").value = featherlink5;
    
    // Grid 6
    const gridlink6 = loadData("gridlink6");
    if(gridlink6 !== null)
        document.getElementById("gridlink6").value = gridlink6;
    
    const featherlink6 = loadData("featherlink6");
    if(featherlink6 !== null)
        document.getElementById("featherlink6").value = featherlink6;
    
    // ------------------ List Links ------------------
    const linklist1 = loadData("linklist1");
    if(linklist1 !== null)
        document.getElementById("linklist1").value = linklist1;
    
    const linklist2 = loadData("linklist2");
    if(linklist2 !== null)
        document.getElementById(clickbutton1()).value = linklist2;
    
    const linklist3 = loadData("linklist3");
    if(linklist3 !== null)
        document.getElementById(clickbutton1()).value = linklist3;
    
    const linklist4 = loadData("linklist4");
    if(linklist4 !== null)
        document.getElementById(clickbutton1()).value = linklist4;
    
    const linklist5 = loadData("linklist5");
    if(linklist5 !== null)
        document.getElementById(clickbutton1()).value = linklist5;
    
    const linklist6 = loadData("linklist6");
    if(linklist6 !== null)
        document.getElementById(clickbutton1()).value = linklist6;
    
    const linklist7 = loadData("linklist7");
    if(linklist7 !== null)
        document.getElementById(clickbutton1()).value = linklist7;
    
    const linklist8 = loadData("linklist8");
    if(linklist8 !== null)
        document.getElementById(clickbutton1()).value = linklist8;
    
    const linklist9 = loadData("linklist9");
    if(linklist9 !== null)
        document.getElementById(clickbutton1()).value = linklist9;
    
    // ------------------ List Links 2 ------------------
    const link2list1 = loadData("link2list1");
    if(link2list1 !== null)
        document.getElementById("link2list1").value = link2list1;
    
    const link2list2 = loadData("link2list2");
    if(link2list2 !== null)
        document.getElementById(clickbutton2()).value = link2list2;
    
    const link2list3 = loadData("link2list3");
    if(link2list3 !== null)
        document.getElementById(clickbutton2()).value = link2list3;
    
    const link2list4 = loadData("link2list4");
    if(link2list4 !== null)
        document.getElementById(clickbutton2()).value = link2list4;
    
    const link2list5 = loadData("link2list5");
    if(link2list5 !== null)
        document.getElementById(clickbutton2()).value = link2list5;
    
    const link2list6 = loadData("link2list6");
    if(link2list6 !== null)
        document.getElementById(clickbutton2()).value = link2list6;
    
    const link2list7 = loadData("link2list7");
    if(link2list7 !== null)
        document.getElementById(clickbutton2()).value = link2list7;
    
    const link2list8 = loadData("link2list8");
    if(link2list8 !== null)
        document.getElementById(clickbutton2()).value = link2list8;
    
    const link2list9 = loadData("link2list9");
    if(link2list9 !== null)
        document.getElementById(clickbutton2()).value = link2list9;
    
    // Feather Icons List 1
    const linkicon1 = loadData("linkicon1");
    if(linkicon1 !== null)
        document.getElementById("linkicon1").value = linkicon1;
    
    // Feather Icons List 2
    const linkicon2 = loadData("linkicon2");
    if(linkicon2 !== null)
        document.getElementById("linkicon2").value = linkicon2;
}

/**
 * function to fill with mock data.
 */
function fillwithMockData(){
    SetData("username", "John Doe");
    SetData("apikey", "12345678");
    SetData("latitude", "40.7128");
    SetData("longitude", "74.0060");
    SetData("icons", "Nord");
    SetData("unit", "C");
    SetData("hover", "yes");
    SetData("accentcolor", "#4b8ec4");
    SetData("accentcolor2", "#FFFFFF");
    SetData("font-family", "Open Sans");
    SetData("font-url", "https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700&display=swap");
    SetData("timeformat", "24");
    SetData("gridlink1", "https://www.youtube.com");
    SetData("featherlink1", "youtube");
    SetData("gridlink2", "https://www.twitch.tv");
    SetData("featherlink2", "twitch");
    SetData("gridlink3", "https://www.instagram.com");
    SetData("featherlink3", "instagram");
    SetData("gridlink4", "https://twitter.com");
    SetData("featherlink4", "twitter");
    SetData("gridlink5", "https://www.linkedin.com");
    SetData("featherlink5", "linkedin");
    SetData("gridlink6", "https://www.gitlab.com");
    SetData("featherlink6", "gitlab");
    SetData("linkicon1", "loader");
    SetData("linkicon2", "loader");
    SetData("linklist1", "Gmail,https://mail.google.com");
    SetData("link2list1", "Github,https://github.com");
}

if(loadData("configured") == null){
    SetData("configured", "true");
    fillwithMockData();
}

loadSettings();

function download(filename, textInput) {
    let element = document.createElement('a');
    element.setAttribute('href','data:text/plain;charset=utf-8, ' + encodeURIComponent(textInput));
    element.setAttribute('download', filename);
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
}

document.getElementById("export").addEventListener("click", () => {
    let userdata = JSON.stringify(JSON.stringify(localStorage));
    download("bento-settings-export.csv", userdata);
});

document.getElementById("import").addEventListener("change", (e) => {
    const file = e.target.files[0];
    dosettings(file);
});

async function dosettings(file){
    try {
        let filecontent = await readFile(file);
        var data = JSON.parse(JSON.parse(filecontent));
        Object.keys(data).forEach(function (k) {
            localStorage.setItem(k, data[k]);
        });
        window.location.href = "options.html?imported=true";
    } catch (error) {
        showToast("Invalid Import! Try again.");
    }
}

if(window.location.href.includes("imported=true")){
    showToast("Successfully imported settings.", "green");
    window.history.pushState("","","options.html");
    loadStuff();
}

async function readFile(file){
    return file.text();
}

function loadStuff(){
    var css = `body{--accent: ${loadData("accentcolor")}!important;--sfg: ${loadData("accentcolor2")}!important;--font-family: "${loadData("font-family")}"!important;}`;
    var style = document.createElement('style');
    
    if (style.styleSheet) {
        style.styleSheet.cssText = css;
    } else {
        style.appendChild(document.createTextNode(css));
    }
    
    document.getElementsByTagName('head')[0].appendChild(style);

    // import font-family css
    var fontcss = document.createElement('link');
    fontcss.rel = "stylesheet";
    fontcss.href = loadData("font-url");
    document.getElementsByTagName('head')[0].appendChild(fontcss);
}

loadStuff();

function showAlert(){
    alert("Head over to fonts.google.com and choose a font. Afterwards, copy the link and paste it into the font-url field. It should look like this: https://fonts.googleapis.com/css2?family=Open+Sans");
}