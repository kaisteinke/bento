/**
 * 
 * @param {string} key the name of the variable you want to receive
 * @returns {string} data
 */
 function loadData(key){
    return localStorage.getItem(key);
}

/**
 * 
 * @param {string} key the name of the variable
 * @param {string} value the value of the variables
 */
function SetData(key, value){
    localStorage.setItem(key, value);
}

/**
 * function to fill with mock data.
 */
 function fillwithMockData(){
    SetData("username", "John Doe");
    SetData("apikey", "12345678");
    SetData("latitude", "40.7128");
    SetData("longitude", "74.0060");
    SetData("icons", "Nord");
    SetData("unit", "C");
    SetData("hover", "yes");
    SetData("accentcolor", "#4b8ec4");
    SetData("accentcolor2", "#FFFFFF");
    SetData("font-family", "Open Sans");
    SetData("font-url", "https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700&display=swap");
    SetData("timeformat", "24");
    SetData("gridlink1", "https://www.youtube.com");
    SetData("featherlink1", "youtube");
    SetData("gridlink2", "https://www.twitch.tv");
    SetData("featherlink2", "twitch");
    SetData("gridlink3", "https://www.instagram.com");
    SetData("featherlink3", "instagram");
    SetData("gridlink4", "https://twitter.com");
    SetData("featherlink4", "twitter");
    SetData("gridlink5", "https://www.linkedin.com");
    SetData("featherlink5", "linkedin");
    SetData("gridlink6", "https://www.gitlab.com");
    SetData("featherlink6", "gitlab");
    SetData("linkicon1", "loader");
    SetData("linkicon2", "loader");
    SetData("linklist1", "Gmail,https://mail.google.com");
    SetData("link2list1", "Github,https://github.com");
}

if(loadData("configured") == null){
    SetData("configured", "true");
    fillwithMockData();
}

if(loadData("font-family") == null){
    SetData("font-family", "Open Sans");
    SetData("font-url", "https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700&display=swap");
}
