/**
 * 
 * @param {string} key the name of the variable you want to receive
 * @returns {string} data
 */
 function loadData(key){
  return localStorage.getItem(key);
}

// Get the hour
const today = new Date();
const hour = today.getHours();

// Here you can change your name
const name = loadData("username");

// Here you can change your greetings
const gree1 = 'Go to Sleep, ';
const gree2 = 'Good morning, ';
const gree3 = 'Good afternoon, ';
const gree4 = 'Good evening, ';

// Define the hours of the greetings
if (hour >= 23 || hour < 6) {
  document.getElementById('greetings').innerText = gree1 + name;
} else if (hour >= 6 && hour < 12) {
  document.getElementById('greetings').innerText = gree2 + name;
} else if (hour >= 12 && hour < 17) {
  document.getElementById('greetings').innerText = gree3 + name;
} else {
  document.getElementById('greetings').innerText = gree4 + name;
}
