/**
 * 
 * @param {string} key the name of the variable you want to receive
 * @returns {string} data
 */
 function loadData(key){
    return localStorage.getItem(key);
}

displaylinks();

function displaylinks(){
    // Grid
    displaylink(1, loadData("gridlink1"), loadData("featherlink1"));
    displaylink(2, loadData("gridlink2"), loadData("featherlink2"));
    displaylink(3, loadData("gridlink3"), loadData("featherlink3"));
    displaylink(4, loadData("gridlink4"), loadData("featherlink4"));
    displaylink(5, loadData("gridlink5"), loadData("featherlink5"));
    displaylink(6, loadData("gridlink6"), loadData("featherlink6"));

    // Hover Link Effect

    // List
    // ------------------ List Links ------------------
    const linklist1 = loadData("linklist1");
    if(linklist1 !== null && linklist1 !== ""){
        document.getElementById("linklist1").href = linklist1.split(",")[1];
        document.getElementById("linklist1").innerHTML = linklist1.split(",")[0];
    }
    
    const linklist2 = loadData("linklist2");
    if(linklist2 !== null && linklist2 !== ""){
        document.getElementById("linklist2").href = linklist2.split(",")[1];
        document.getElementById("linklist2").innerHTML = linklist2.split(",")[0];
    }
    
    const linklist3 = loadData("linklist3");
    if(linklist3 !== null && linklist3 !== ""){
        document.getElementById("linklist3").href = linklist3.split(",")[1];
        document.getElementById("linklist3").innerHTML = linklist3.split(",")[0];
    }

    const linklist4 = loadData("linklist4");
    if(linklist4 !== null && linklist4 !== ""){
        document.getElementById("linklist4").href = linklist4.split(",")[1];
        document.getElementById("linklist4").innerHTML = linklist4.split(",")[0];
    }

    const linklist5 = loadData("linklist5");
    if(linklist5 !== null && linklist5 !== ""){
        document.getElementById("linklist5").href = linklist5.split(",")[1];
        document.getElementById("linklist5").innerHTML = linklist5.split(",")[0];
    }

    const linklist6 = loadData("linklist6");
    if(linklist6 !== null && linklist6 !== ""){
        document.getElementById("linklist6").href = linklist6.split(",")[1];
        document.getElementById("linklist6").innerHTML = linklist6.split(",")[0];
    }

    const linklist7 = loadData("linklist7");
    if(linklist7 !== null && linklist7 !== ""){
        document.getElementById("linklist7").href = linklist7.split(",")[1];
        document.getElementById("linklist7").innerHTML = linklist7.split(",")[0];
    }

    const linklist8 = loadData("linklist8");
    if(linklist8 !== null && linklist8 !== ""){
        document.getElementById("linklist8").href = linklist8.split(",")[1];
        document.getElementById("linklist8").innerHTML = linklist8.split(",")[0];
    }

    const linklist9 = loadData("linklist9");
    if(linklist9 !== null && linklist9 !== ""){
        document.getElementById("linklist9").href = linklist9.split(",")[1];
        document.getElementById("linklist9").innerHTML = linklist9.split(",")[0];
    }
    
    // ------------------ List Links 2 ------------------
    const link2list1 = loadData("link2list1");
    if(link2list1 !== null && link2list1 !== ""){
        document.getElementById("link2list1").href = link2list1.split(",")[1];
        document.getElementById("link2list1").innerHTML = link2list1.split(",")[0];
    }

    const link2list2 = loadData("link2list2");
    if(link2list2 !== null && link2list2 !== ""){
        document.getElementById("link2list2").href = link2list2.split(",")[1];
        document.getElementById("link2list2").innerHTML = link2list2.split(",")[0];
    }

    const link2list3 = loadData("link2list3");
    if(link2list3 !== null && link2list3 !== ""){
        document.getElementById("link2list3").href = link2list3.split(",")[1];
        document.getElementById("link2list3").innerHTML = link2list3.split(",")[0];
    }

    const link2list4 = loadData("link2list4");
    if(link2list4 !== null && link2list4 !== ""){
        document.getElementById("link2list4").href = link2list4.split(",")[1];
        document.getElementById("link2list4").innerHTML = link2list4.split(",")[0];
    }

    const link2list5 = loadData("link2list5");
    if(link2list5 !== null && link2list5 !== ""){
        document.getElementById("link2list5").href = link2list5.split(",")[1];
        document.getElementById("link2list5").innerHTML = link2list5.split(",")[0];
    }

    const link2list6 = loadData("link2list6");
    if(link2list6 !== null && link2list6 !== ""){
        document.getElementById("link2list6").href = link2list6.split(",")[1];
        document.getElementById("link2list6").innerHTML = link2list6.split(",")[0];
    }
    
    const link2list7 = loadData("link2list7");
    if(link2list7 !== null && link2list7 !== ""){
        document.getElementById("link2list7").href = link2list7.split(",")[1];
        document.getElementById("link2list7").innerHTML = link2list7.split(",")[0];
    }

    const link2list8 = loadData("link2list8");
    if(link2list8 !== null && link2list8 !== ""){
        document.getElementById("link2list8").href = link2list8.split(",")[1];
        document.getElementById("link2list8").innerHTML = link2list8.split(",")[0];
    }

    const link2list9 = loadData("link2list9");
    if(link2list9 !== null && link2list9 !== ""){
        document.getElementById("link2list9").href = link2list9.split(",")[1];
        document.getElementById("link2list9").innerHTML = link2list9.split(",")[0];
    }
    
    // Feather Icons List 1
    const linkicon1 = loadData("linkicon1");
    if(linkicon1 !== null && linkicon1 !== "")
        document.getElementById("linkicon1").setAttribute("data-feather", linkicon1);
    
    // Feather Icons List 2
    const linkicon2 = loadData("linkicon2");
    if(linkicon2 !== null && linkicon2 !== "")
        document.getElementById("linkicon2").setAttribute("data-feather", linkicon2);
}

function displaylink(num, url, icon){
    document.getElementsByClassName(`buttonLink__link-${num}`)[0].href = url;
    if(icon !== null && icon !== "")
        document.getElementsByClassName(`buttonLink__link-${num}`)[0].firstElementChild.setAttribute("data-feather", icon);
    else
        document.getElementsByClassName(`buttonLink__link-${num}`)[0].firstElementChild.setAttribute("data-feather", "loader");
    
    if(url.includes("youtube.com")){
        document.getElementsByClassName(`buttonLink__link-${num}`)[0].classList.add("youtube");
    }
    if(url.includes("twitch.tv")){
        document.getElementsByClassName(`buttonLink__link-${num}`)[0].classList.add("twitch");
    }
    if(url.includes("linkedin.com")){
        document.getElementsByClassName(`buttonLink__link-${num}`)[0].classList.add("linkedin");
    }
    if(url.includes("twitter.com")){
        document.getElementsByClassName(`buttonLink__link-${num}`)[0].classList.add("twitter");
    }
    if(url.includes("gitlab.com")){
        document.getElementsByClassName(`buttonLink__link-${num}`)[0].classList.add("gitlab");
    }
    if(url.includes("instagram.com")){
        document.getElementsByClassName(`buttonLink__link-${num}`)[0].classList.add("instagram");
    }
    if(url.includes("whatsapp.com")){
        document.getElementsByClassName(`buttonLink__link-${num}`)[0].classList.add("whatsapp");
    }
    if(url.includes("facebook.com")){
        document.getElementsByClassName(`buttonLink__link-${num}`)[0].classList.add("facebook");
    }
    if(url.includes("reddit.com")){
        document.getElementsByClassName(`buttonLink__link-${num}`)[0].classList.add("reddit");
    }if(url.includes("amazon.com") || url.includes("amazon.de")){
        document.getElementsByClassName(`buttonLink__link-${num}`)[0].classList.add("amazon");
    }
    if(url.includes("github.com")){
        document.getElementsByClassName(`buttonLink__link-${num}`)[0].classList.add("github");
    }
}

var els = document.getElementsByClassName("list__link");
Array.prototype.forEach.call(els, function(element) {
    if(element.innerHTML == ""){
        element.classList.add("hidden");
    }
});

var css = `body{--accent: ${loadData("accentcolor")}!important;--sfg: ${loadData("accentcolor2")}!important;--font-family: "${loadData("font-family")}"!important;}`;
var style = document.createElement('style');

if (style.styleSheet) {
    style.styleSheet.cssText = css;
} else {
    style.appendChild(document.createTextNode(css));
}

document.getElementsByTagName('head')[0].appendChild(style);

if(loadData("hover") == "no"){
    document.getElementById("cardlist1").classList.add("no-hover");
    document.getElementById("cardlist2").classList.add("no-hover");
}

// import font-family css
var fontcss = document.createElement('link');
fontcss.rel = "stylesheet";
fontcss.href = loadData("font-url");
document.getElementsByTagName('head')[0].appendChild(fontcss);