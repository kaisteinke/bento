<div align="center">
    <h1>🍱 Bento</h1>
    <b>A Clean and Simple Startpage <a href="https://chrome.google.com/webstore/detail/bento-new-tab-page/bdhpicdiahfbfeejiamoejihononngei">Download Now</a></b>
</div>

<p align="center">
  <img src="assets/Header.png">
</p>

## Index

- [Features](#features)
- [Usage](#usage)
- [Customization](#customization)
  - [Weather Info](#weather-info)
  - [Weather Icons](#weather-icons)
- [Support](#support)
- [Credits](#credits)

### Features:

- **Dark/Light** mode, you can toggle them and It'll be saved in local storage.
- **Clock and Date** format can be set to 24 hour (default) or 12 hour.
- **Icons** all icons are from Feather Icons
- **Customizable** Everything can be configured in the [Settings Page](#customization)

### Usage:

Install the extension [here](https://chrome.google.com/webstore/detail/bento-new-tab-page/bdhpicdiahfbfeejiamoejihononngei) and configure it to your needs in the settings menu

### Customization

You can change the links (and the icons too) in the Settings Page

<p align="center">
  <img src="assets/SubHeader.png">
</p>

### Weather Info

For setting up the Weather widget you're going to need an API Key in: [openweathermap.org](https://openweathermap.org/). Once you have your Key you'll need to set yourlatitude and longitude, you can use: [latlong.net](https://www.latlong.net/) to get them. Then you just have to fill them in the settings page

### Weather Icons

<p align="center">
  <img src="assets/previewico.png">
</p>

The weather icons are based in Feather icons and I made 4 sets of them:

- **OneDark** (_Default one_) Using the One Dark Pro color scheme
- **Nord** Using the Nord Color Scheme and easy-to-eyes colors
- **White** For Dark theme only users that want a minimalist look
- **Dark** For White theme only users that want a minimalist look

These are all easily customizable in the settings menu

<p align="center">
  <img src="assets/settings-menu.png">
</p>

# Support
Support my work by buying me a coffee!

[![Buy Me A Coffee](img/buymeacoffee.jpg)](https://buymeacoffee.com/kaisteinke)

# Credits

*  Wallpaper by [anima_contritum](https://www.reddit.com/r/wallpaper/comments/krmlzg/made_some_minimalist_wallpapers_inspired_by_the/?utm_source=share&utm_medium=web2x&context=3)
* Inspired by [migueravila](https://github.com/migueravila)
* Icons: [Feather Icons](https://feathericons.com/)